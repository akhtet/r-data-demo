# script to show sample data manipulations

# use case

# Get list of contracts from PMDS and calculate MTM using price from EMDM
source("init_db_connection.R")
pmds_db <- init_db_connection(server="MPSQLSP2",
                              database = "PMDS_PROD")

pmds_contracts <- sqlQuery(pmds_db,"select * from dbo.elec_cont_summary_cur_v")

# check the table
dim(pmds_contracts)
names(pmds_contracts)
sapply(seq(1,ncol(pmds_contracts)),function(x){class(pmds_contracts[,x])})

# convert START_DATE and END_DATE into Date
pmds_contracts$START_DATE_dt <- as.Date(pmds_contracts$START_DATE)
pmds_contracts$END_DATE_dt <- as.Date(pmds_contracts$END_DATE)
# add substring of profile as a column
pmds_contracts$CONTRACT_PROFILE_s <- substring(pmds_contracts$CONTRACT_PROFILE,1,1)
# all good

# get emdm_price
emdm_db <- init_db_connection(server = "mptsqvp1",
                              database = "EMDM_DATAMART")
emdm_price <- sqlQuery(emdm_db,"select * from price.ELEC_GFI_MARKET_CURVE_DAY_cur_v where ")

# check the table
dim(emdm_price)
names(emdm_price)
sapply(seq(1,ncol(emdm_price)),function(x){class(emdm_price[,x])})

# convert DATE, FROM_DATE, TO_DATE to Date format
emdm_price$DATE_dt <- as.Date(emdm_price$DATE)
emdm_price$FROM_DATE_dt <- as.Date(emdm_price$FROM_DATE)
emdm_price$TO_DATE_dt <- as.Date(emdm_price$TO_DATE)
# create flag for PERIOD_RANGE like '%Q%'
# filter emdm price
period_range_filter <- function(x)
  ifelse(length(grep("Q",x))==0,F,T)

emdm_price$PERIOD_RANGE_FILTER <- sapply(emdm_price$PERIOD_RANGE,
                                         period_range_filter)
table(emdm_price$PERIOD_RANGE_FILTER)

# filter pmds_contracts
contracts_filter <- which(pmds_contracts$INSTRUMENT == 'Futures - Swap' & 
                            as.Date(pmds_contracts$END_DATE_dt) >= as.Date('2016-11-28'))
pmds_contracts_filtered <- pmds_contracts[contracts_filter,]

# filter emdm_price
table(emdm_price$PRODUCT)
table(emdm_price$REGION_CODE)
table(emdm_price$AFMA)

emdm_price_filter <- which(emdm_price$PERIOD_RANGE_FILTER & 
                             !is.na(emdm_price$REGION_CODE) &
                             emdm_price$PRODUCT == 'SWAP' & 
                             emdm_price$AFMA == 'Y')



emdm_price_filtered <- emdm_price[emdm_price_filter,]


# do the merge
mtm_price <- merge(x = pmds_contracts_filtered,
                   y = emdm_price_filtered,
                   by.x = c("START_DATE_dt","END_DATE_dt","CONTRACT_PROFILE_s"),
                   by.y = c("FROM_DATE_dt","TO_DATE_dt","POP"),
                   all.x = T)
dim(mtm_price)
names(mtm_price)

# calculate MTM column
mtm_price$MTM <- ifelse(mtm_price$AVE_VOLUME<0,-1,1)*mtm_price$ENERGY

# keep only relevant columns
keep_columns <- c(1:2,4:36,46,51)
mtm_price <- mtm_price[,keep_columns]
odbcCloseAll()
